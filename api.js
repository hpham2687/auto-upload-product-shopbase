const { default: axios } = require("axios");
const CONFIG = require("./config");
const path = require("path");
const fs = require("fs");

const headers = {
  "x-shopbase-access-token": CONFIG.SHOPBASE_TOKEN,
  "Content-Type": "application/json",
  Cookie:
    "crisp-client%2Fsession%2Ff5c7331c-510d-4a08-bf62-8c63aeeb568e=session_8f32fef5-5668-4b57-9b4c-55fefca87ac6",
};

const postProduct = (productData) => {
  var config = {
    method: "post",
    url: `https://${CONFIG.SHOP_USERNAME}.onshopbase.com/admin/products.json`,
    headers,
    data: productData,
  };

  axios(config)
    .then(async function (response) {
      // console.log(JSON.stringify(response.data));
      let product = response.data.product;
      let productId = product.id;
      // console.log(product);
      let productVariantsData = product.variants.map((variant) => ({
        variantId: variant.id,
        option2: variant.option2,
      }));

      // console.log(productVariantsData);

      console.log(productId + "is created");
      uploadImgToProduct(productVariantsData, productId);
      // postImageToShopbase(product);
    })
    .catch(function (error) {
      console.log(error);
    });
};
const postSaveImgToVariant = (productId, data) => {
  var config = {
    method: "put",
    url: `https://${CONFIG.SHOP_USERNAME}.onshopbase.com/admin/products/${productId}/images.json`,
    headers,
    data: data,
  };

  return axios(config);
};

const postImgToServer = (productId, imgBase64, data) => {
  var data = JSON.stringify({ image: { attachment: imgBase64 } });

  var config = {
    method: "post",
    url: `https://${CONFIG.SHOP_USERNAME}.onshopbase.com/admin/products/${productId}/images.json`,
    headers,
    data: data,
  };

  return axios(config);
};

async function uploadImgToProduct(productVariantsData, productId) {
  console.log("Uploading images to server...");
  let imgIdAndItsOptionName = {};

  let files = fs.readdirSync(CONFIG.IMG_DIR);
  const imgFiles = files.filter(
    (el) => path.extname(el) === ".png" || path.extname(el) === ".jpeg"
  );
  // upload img to server
  await Promise.all(
    imgFiles.map(async function (img, index) {
      //   if (index > 0) return;
      const imgInBase64 = fs.readFileSync(`./images/${img}`, {
        encoding: "base64",
      });
      console.log(`Uploading ${img}...`);
      try {
        let postImg = await saveImgToServer(productId, imgInBase64);
        // console.log(JSON.stringify(postImg.data));
        let imgId = postImg.data.image.id;
        // img is option name
        let imgNameWithOutExtension = img.split(".")[0];
        imgIdAndItsOptionName[imgNameWithOutExtension] = imgId;
      } catch (error) {
        console.log(error);

        console.log("error uploading " + img);
      }
    })
  );

  console.log(imgIdAndItsOptionName);
  // post img uploaded to variant
  for (const [key, value] of Object.entries(imgIdAndItsOptionName)) {
    let listVarians = productVariantsData.filter(
      (variant) => variant.option2 === key
    );
    console.log(listVarians);
    await Promise.all(
      listVarians.map(async (variant) => {
        console.log(
          `Posting variantId ${variant.variantId} - ${variant.option2} `
        );

        let saveVariant = await saveImgToVariant(
          productId,
          variant.variantId,
          imgIdAndItsOptionName[key]
        );
        await sleep();
        console.log(JSON.stringify(saveVariant.data));
        console.log(
          `end Posting variantId ${variant.variantId} - ${variant.option2} `
        );
      })
    );
  }

  return imgIdAndItsOptionName;
}
function saveImgToServer(productId, imgBase64) {
  var data = JSON.stringify({ image: { attachment: imgBase64 } });

  return postImgToServer(productId, imgBase64, data);
}

async function saveImgToVariant(productId, variantId, imgId) {
  console.log("data post=");
  var data = JSON.stringify({
    is_product_print_base: false,
    variant_images: [{ variant_id: variantId, image_ids: [imgId] }],
  });

  // console.log(data);

  //   console.log(variantId, imgId);
  return postSaveImgToVariant(productId, data);
}

async function sleep() {
  await timeout(1000);
}
function timeout(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

module.exports = { postImgToServer, postSaveImgToVariant, postProduct };

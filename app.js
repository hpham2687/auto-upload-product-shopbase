const axios = require("axios").default;
const { postSaveImgToVariant, postImgToServer, postProduct } = require("./api");
const { getPermutations, generateProductDataObj } = require("./utils");
const CONFIG = require("./config");
let i = 0;
let dataFromFileObj = {
  variants: [],
};
let priceData = {};

// read all lines:
const fs = require("fs");

try {
  // read contents of the file
  const data = fs.readFileSync(CONFIG.DATA_DIR, "UTF-8");

  // split the contents by new line
  const lines = data.split(/\r?\n/);

  //   print all lines
  lines.forEach((line) => {
    if (i == 0) {
      // product name
      dataFromFileObj.title = line;
    } else if (i == 1 || i == 2) {
      // variant 1
      let variantName = line.split(":")[0];

      let variantValues = line.split(":")[1];
      variantValues = variantValues.split(",");
      dataFromFileObj.variants.push({
        variantName,
        variantValues,
        belongTo: variantName,
      });
      if (i == 2)
        variantValues.forEach((variantValue) => {
          // save variant option 2 values, use to assign price below
          priceData[variantValue] = null;
        });
    } else {
      // price here
      let prices = line.split(",");
      let i = 0;
      for (const [key, value] of Object.entries(priceData)) {
        console.log(`${key}: ${value}`);
        priceData[key] = prices[i];
        i++;
      }
      console.log(priceData);
    }
    i++;
  });
  let variantPermutations = getPermutations(
    dataFromFileObj.variants.map((item) => item.variantValues)
  );

  let data2Send = generateProductDataObj(
    {
      title: dataFromFileObj.title,
      variantPermutations,
      options: dataFromFileObj.variants.map((item) => ({
        name: item.variantName,
        values: item.variantValues,
      })),
    },
    priceData
  );
  fs.writeFile("post.txt", JSON.stringify(data2Send), function (err) {
    if (err) return console.log(err);
    console.log("Hello World > helloworld.txt");
  });

  saveProduct(data2Send);
  //uploadImgToProduct("dfd");
} catch (err) {
  console.error(err);
}

function saveProduct(productData) {
  return postProduct(productData);
}

function getPermutations(arrayOfArrays) {
  let permutations = [];
  let remainder, permutation;
  let permutationCount = 1;
  let placeValue = 1;
  let placeValues = new Array(arrayOfArrays.length);
  for (let i = arrayOfArrays.length - 1; i >= 0; i--) {
    placeValues[i] = placeValue;
    placeValue *= arrayOfArrays[i].length;
  }
  permutationCount = placeValue;
  for (let i = 0; i < permutationCount; i++) {
    remainder = i;
    permutation = [];
    for (let j = 0; j < arrayOfArrays.length; j++) {
      permutation[j] = arrayOfArrays[j][Math.floor(remainder / placeValues[j])];
      remainder = remainder % placeValues[j];
    }
    permutations.push(
      permutation.reduce(
        (prev, curr, currentIndex) =>
          prev + (currentIndex == 1 ? " / " : "") + curr,
        ""
      )
    );
  }
  return permutations;
}

function generateProductDataObj(
  { title, options, variantPermutations },
  priceData
) {
  return {
    product: {
      images: [],
      collections: [],
      options,
      variants: variantPermutations.map((variantData, index) => {
        let option1 = variantData.split(" / ")[0];
        let option2 = variantData.split(" / ")[1];
        let price = priceData[option2];
        let title = variantData;
        let position = index + 1;

        let aVariantData = {
          option1,
          option2,
          price,
          title,
          position,
        };

        return generateAVariant(aVariantData);
      }),
      body_html: "",
      title: title,
      metafields_global_title_tag: "",
      metafields_global_description_tag: "",
      custom_options: [],
      variantDefault: {
        active: true,
        option1: "",
        price: 0,
        sku: "",
        compare_at_price: 0,
        cost_per_item: 0,
        bar_code: 0,
        weight: 0,
        is_default: false,
        weight_unit: "lb",
        inventory_quantity: 0,
        inventory_management: "",
        inventory_policy: "continue",
        requires_shipping: true,
        taxable: true,
        fulfillment_service: "shopbase",
        variant_images: [],
      },
      tags: "",
      product_availability: 1,
    },
  };
}

function generateAVariant({ option1, option2, price, title, position }) {
  return {
    active: true,
    option1: option1,
    price: price,
    sku: "",
    compare_at_price: 0,
    cost_per_item: 0,
    bar_code: 0,
    weight: 0,
    weight_unit: "lb",
    inventory_quantity: 0,
    inventory_management: "",
    inventory_policy: "continue",
    requires_shipping: true,
    taxable: true,
    fulfillment_service: "shopbase",
    variant_images: [],
    option2: option2,
    title: title,
    position: position,
  };
}

module.exports = { getPermutations, generateProductDataObj };
